#!/bin/bash

set -e

echo "Enable cloud apis..."
declare -a apis=( "servicenetworking" "cloudresourcemanager" "container" "sourcerepo" "compute" "servicemanagement" "storage-api" )
for api in "${apis[@]}"; do
    gcloud services enable "${api}.googleapis.com"
done

gcloud projects list
PROJECT_ID=$(gcloud config get-value project)

read -p "Please enter your PROJECT_ID [${PROJECT_ID}]: " GCLOUD_PROJECT
GCLOUD_PROJECT=${GCLOUD_PROJECT:-$PROJECT_ID}
echo $GCLOUD_PROJECT

# Required IAM permissions
# todo falta member
# CLOUD_BUILD_ACCOUNT=$(gcloud projects get-iam-policy $GCLOUD_PROJECT --filter="(bindings.role:roles/cloudbuild.builds.builder)"  --flatten="bindings[].members" --format="value(bindings.members[])")

gcloud iam service-accounts list
echo -n "Do you want to create a new service account? (Please enter between 6 and 30 characteres)[y/n]"
read answer

if [ "$answer" != "${answer#[Yy]}" ]; then
        echo "Insert your service account name"
        read SERVICE_ACCOUNT_NAME
        gcloud iam service-accounts create $SERVICE_ACCOUNT_NAME
else
        echo "Insert your service account name"
        read SERVICE_ACCOUNT_NAME
fi

echo $SERVICE_ACCOUNT_NAME

CLOUD_BUILD_ACCOUNT="serviceAccount:${SERVICE_ACCOUNT_NAME}@${GCLOUD_PROJECT}.iam.gserviceaccount.com"


# Add the editor role to the account 
gcloud projects add-iam-policy-binding $GCLOUD_PROJECT \
  --member $CLOUD_BUILD_ACCOUNT \
  --role roles/editor \
  --role roles/compute.instanceAdmin \
  --role roles/compute.admin \
  --role roles/editor \
  --role roles/iam.serviceAccountUser

gcloud iam service-accounts keys create "auth.json" --iam-account=${SERVICE_ACCOUNT_NAME}@${GCLOUD_PROJECT}.iam.gserviceaccount.com

cp auth.json ./packer/auth.json